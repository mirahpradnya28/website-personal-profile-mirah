<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="copyright" content="MACode ID, https://www.macodeid.com/">

  <title>Personal Profile Mirah</title>

  <link rel="shortcut icon" href="../assets/img/logo.png" type="image/x-icon">

  <link rel="stylesheet" type="text/css" href="../assets/css/themify-icons.css">

  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css"> 

  <link rel="stylesheet" type="text/css" href="../assets/vendor/animate/animate.css">

  <link rel="stylesheet" type="text/css" href="../assets/vendor/owl-carousel/owl.carousel.css">

  <link rel="stylesheet" type="text/css" href="../assets/vendor/perfect-scrollbar/css/perfect-scrollbar.css">

  <link rel="stylesheet" type="text/css" href="../assets/vendor/nice-select/css/nice-select.css">

  <link rel="stylesheet" type="text/css" href="../assets/vendor/fancybox/css/jquery.fancybox.min.css">

  <link rel="stylesheet" type="text/css" href="../assets/css/virtual.css">

  <link rel="stylesheet" type="text/css" href="../assets/css/topbar.virtual.css">
</head>

@if ($page==1)
    <body class="beranda">
        @elseif ($page==2)

        <body class="tentang">
            @elseif ($page==3)

            <body class="skill">
                @elseif ($page==4)

                <body class="pendidikan">
                    @elseif ($page==5)

                    <body class="galeri">
                        @elseif ($page==6)

                        <body class="kontak">
                            @endif

<header class="theme-orange">
  <!-- Head -->
  <div class="vg-page page-home" id="home" style="background-image: url(../assets/img/galeri/beranda.jpg)">
    <!-- Navbar -->
    <div class="navbar navbar-expand-lg navbar-dark sticky" data-offset="500">
      <div class="container">
        <a href="" class="navbar-brand">Biodata</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#main-navbar" aria-expanded="true">
          <span class="ti-menu"></span>
        </button>

        <div class="collapse navbar-collapse" id="main-navbar">
          <ul class="navbar-nav ml-auto">
              @if ($page==1)
          <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('beranda')}}" class="nav-link" >Beranda</a>
            </li>
            @if ($page==2)
            <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('tentang')}}" class="nav-link">Tentang</a>
            </li>
            @if ($page==3)
            <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('skill')}}" class="nav-link">Skill</a>
            </li>
            @if ($page==4)
            <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('pendidikan')}}" class="nav-link">Pendidikan & Pengalaman</a>
            </li>
            @if ($page==5)
            <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('galeri')}}" class="nav-link" >Galeri</a>
            </li>
            @if ($page==6)
            <li class="nav-item active">
              @else
              <li class="nav-item">
              @endif
              <a href="{{route('kontak')}}" class="nav-link" >Kontak</a>
            </li>
          </ul>
        </div>
      </div>
    </div> <!-- End Navbar -->
    @yield('konten') 

<script src="../assets/js/jquery-3.5.1.min.js"></script>

<script src="../assets/js/bootstrap.bundle.min.js"></script>

<script src="../assets/vendor/owl-carousel/owl.carousel.min.js"></script>

<script src="../assets/vendor/perfect-scrollbar/js/perfect-scrollbar.js"></script>

<script src="../assets/vendor/isotope/isotope.pkgd.min.js"></script>

<script src="../assets/vendor/nice-select/js/jquery.nice-select.min.js"></script>

<script src="../assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>

<script src="../assets/vendor/wow/wow.min.js"></script>

<script src="../assets/vendor/animateNumber/jquery.animateNumber.min.js"></script>

<script src="../assets/vendor/waypoints/jquery.waypoints.min.js"></script>

<!-- <script src="../assets/js/topbar-virtual.js"></script> -->


</html>