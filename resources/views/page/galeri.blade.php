@extends('master.app')
@section('konten')

    <!-- Caption header -->
    <div class="caption-header text-center wow zoomInDown">
      <h5 class="fw-normal">GALERI</h5>
      <h1 class="fw-light mb-4">Saya <b class="fg-theme">Mirah</b> Pradnya</h1>
      <div class="badge">Sistem Informasi'19 Undiksha</div>
    </div> <!-- End Caption header -->
  </div>

    <!-- galeri page -->
  <div class="vg-page page-portfolio" id="galeri">
    <div class="container">

      <div class="gridder my-3">
        <div class="grid-item apps wow zoomIn">
          <div class="img-place" data-src="../assets/img/galeri/foto1.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2018</h5> <p>Sanur, Bali</p>">
            <img src="../assets/img/galeri/foto1.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2018</h5>
              <p>Sanur, Bali</p>
            </div>
          </div>
        </div>

        <div class="grid-item template wireframes wow zoomIn">
          <div class="img-place" data-src="../assets/img/galeri/foto2.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2017</h5> <p>Wisuda Undiksha, Bali</p>">
            <img src="../assets/img/galeri/foto2.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2017</h5>
              <p>Wisuda Undiksha, Bali</p>
            </div>
          </div>
        </div>

        <div class="grid-item apps ios wow zoomIn">
          <div class="img-place" data-src="../assets/img/galeri/foto3.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2020</h5> <p>Kendran, Gianyar</p>">
            <img src="../assets/img/galeri/foto3.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2020</h5>
              <p>Kendran, Gianyar</p>
            </div>
          </div>
        </div>

        <div class="grid-item graphic ui-ux wow zoomIn">
          <div class="img-place" data-src="../assets/img/work/foto4.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2017</h5> <p>Sanur, Bali</p>">
            <img src="../assets/img/galeri/foto4.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2017</h5>
              <p>Sanur,Bali</p>
            </div>
          </div>
        </div>

        <div class="grid-item apps ios wow zoomIn">
          <div class="img-place" data-src="../assets/img/galeri/foto5.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2020</h5> <p>Bali Zoo, Gianyar</p>">
            <img src="../assets/img/galeri/foto5.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2020</h5>
              <p>Bali Zoo, Gianyar</p>
            </div>
          </div>
        </div>

        <div class="grid-item graphic ui-ux wireframes wow zoomIn">
          <div class="img-place" data-src="../assets/img/galeri/foto6.jpg" data-fancybox
            data-caption="<h5 class='fg-theme'>2021</h5> <p>Suksesi HMJ TI</p>">
            <img src="../assets/img/galeri/foto6.jpg" alt="">
            <div class="img-caption">
              <h5 class="fg-theme">2021</h5>
              <p>Suksesi HMJ TI</p>
            </div>
          </div>
        </div>
      </div> <!-- End gridder -->
  </div> 
</div>
<!-- End Portfolio page -->

@endsection