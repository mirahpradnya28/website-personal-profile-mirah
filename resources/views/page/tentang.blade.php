@extends('master.app')
@section('konten')

    <!-- Caption header -->
    <div class="caption-header text-center wow zoomInDown">
      <h5 class="fw-normal">Tentang</h5>
      <h1 class="fw-light mb-4">Saya <b class="fg-theme">Mirah</b> Pradnya</h1>
      <div class="badge">Sistem Informasi'19 Undiksha</div>
    </div> <!-- End Caption header -->
    </div>

  <!-- About -->
  <div class="vg-page page-about" id="tentang">
    <div class="container py-5">
      <div class="row">
        <div class="col-lg-4 py-3">
          <div class="img-place wow fadeInUp">
            <img src="../assets/img/mirah.jpeg" alt="">
          </div>
        </div>
        <div class="col-lg-6 offset-lg-1 wow fadeInRight">
          <h1 class="fw-light">Ni Made Mirah Pradnya Pramesti</h1>
          <h5 class="fg-theme mb-3">Mahasiswa Sifors'19</h5>
          <p class="text-muted">Perkenalan nama saya Ni Made Mirah Pradnya Pramesti biasa dipanggil Mirah. Saya merupakan
            mahasiswa program studi Sistem Informasi Universitas Pendidikan Ganesha. Saya menyukai tantangan baru tetapi saya
            juga sangat mudah menyerah apabila keadaan berada tetap di kondisi yang sama.
          </p>
          <ul class="theme-list">
            <li><b>Asal :</b>Karangasem, Bali</li>
            <li><b>Tinggal :</b>Denpasar, Bali</li>
            <li><b>Tempat, Tanggal Lahir:</b>Denpasar, 12 Mei 2001</li>
            <li><b>Umur :</b>20 Tahun</li>
            <li><b>Jenis Kelamin :</b>Perempuan</li>
          </ul>
        </div>
      </div>
</div>
    </div>
  <!-- End About -->

@endsection