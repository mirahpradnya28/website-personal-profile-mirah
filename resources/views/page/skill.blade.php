@extends('master.app')
@section('konten')

    <!-- Caption header -->
    <div class="caption-header text-center wow zoomInDown">
      <h5 class="fw-normal">SKILL (KEAHLIAN)</h5>
      <h1 class="fw-light mb-4">Saya <b class="fg-theme">Mirah</b> Pradnya</h1>
      <div class="badge">Sistem Informasi'19 Undiksha</div>
    </div> <!-- End Caption header -->
    </div>

<!-- Skill -->
    <div class="container py-5">
        <!-- <h1 class="text-center fw-normal wow fadeIn">Skill Saya</h1> -->
        <div class="row py-3">
            <div class="col-md-6">
            <div class="px-lg-3">
                <h4 class="wow fadeInUp">Software Video & Animasi</h4>
                <br>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Adobe After Effect</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 86%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">86%</div>
                </div>
                </div>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Adobe Premiere</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 80%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">80%</div>
                </div>
                </div>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Blender</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 60%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">60%</div>
                </div>
                </div>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Maya</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 40%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">40%</div>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="px-lg-3">
                <h4 class="wow fadeInUp">Software Desain Grafis</h4>
                <br>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Adobe Photoshop</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 80%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">80%</div>
                </div>
                </div>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Adobe Ilustrator</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 85%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">85%</div>
                </div>
                </div>
                <div class="progress-wrapper wow fadeInUp">
                <span class="caption">Corel Draw</span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 90%;" aria-valuenow="75" aria-valuemin="0"
                    aria-valuemax="100">90%</div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <!-- End Skill -->


@endsection