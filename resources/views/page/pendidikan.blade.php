@extends('master.app')
@section('konten')

    <!-- Caption header -->
    <div class="caption-header text-center wow zoomInDown">
      <h5 class="fw-normal">PENDIDIKAN DAN PENGALAMAN</h5>
      <h1 class="fw-light mb-4">Saya <b class="fg-theme">Mirah</b> Pradnya</h1>
      <div class="badge">Sistem Informasi'19 Undiksha</div>
    </div> <!-- End Caption header -->
    </div>
    <!-- Education -->
    <div class="container pt-5">
      <div class="row">
        <div class="col-md-6 wow fadeInRight">
          <h2 class="fw-normal">Pendidikan</h2>
          <ul class="timeline mt-4 pr-md-5">
            <li>
              <div class="title">2007-2013</div>
              <div class="details">
                <h5>Sekolah Dasar</h5>
                <small class="fg-theme">SD Negeri 5 Tonja</small>
              </div>
            </li>
            <li>
              <div class="title">2013-2016</div>
              <div class="details">
                <h5>Sekolah Menengah Pertama</h5>
                <small class="fg-theme">SMP Negeri 12 Denpasar</small>
              </div>
            </li>
            <li>
              <div class="title">2016-2019</div>
              <div class="details">
                <h5>Sekolah Menengah Kejuruan</h5>
                <small class="fg-theme">SMK Negeri 1 Denpasar</small>
                <p>Kompetensi Keahlian Multimedia</p>
              </div>
            </li>
            <li>
              <div class="title">2019-Sekarang</div>
              <div class="details">
                <h5>Perguruan Tinggi Negeri</h5>
                <small class="fg-theme">Universitas Pendidikan Ganesha</small>
                <p>Program Studi Sistem Informasi</p>
              </div>
            </li>
          </ul>
        </div>
        <div class="col-md-6 wow fadeInRight" data-wow-delay="200ms">
          <h2 class="fw-normal">Pengalaman</h2>
          <ul class="timeline mt-4 pr-md-5">
            <li>
              <div class="title">2016-2017</div>
              <div class="details">
                <h5>Vice President Of Finance</h5>
                <small class="fg-theme">SOS.SC SMK Negeri 1 Denpasar</small>
                <p>Menjabat sebagai VP of Finance dalam organisasi kompetisi kewirausahaan yang diadakan oleh Prestasi Junior Indonesia
                  mewakili SMK Negeri 1 Denpasar.
                </p>
              </div>
            </li>
            <li>
              <div class="title">2017</div>
              <div class="details">
                <h5>Praktek Kerja Lapangan</h5>
                <small class="fg-theme">Hope Digital Printing</small>
                <p>Praktek Kerja Lapangan dimulai dari tanggal 9 Oktober-11 November 2017. Pada PKL ini saya sebagai Desainer Grafis
                  yang membantu pelanggan untuk mendesain kartu nama, baliho, dan yang lainnya sebelum masuk ke tahap percetakan.
                </p>
              </div>
            </li>
            <li>
              <div class="title">2017-2018</div>
              <div class="details">
                <h5>Extrakurikuler</h5>
                <small class="fg-theme">Skensa Jurnalist</small>
                <p>Pada tahun 2017-2018 saya dipercayai untuk menjadi Wakil Ketua Ekstrakurikuler Jurnalistik di SMK Negeri 1 Denpasar
                </p>
              </div>
            </li>
            <li>
              <div class="title">2018</div>
              <div class="details">
                <h5>Praktek Kerja Lapangan</h5>
                <small class="fg-theme">Emotions Art Movie</small>
                <p>Praktek Kerja Lapangan dimulai dari tanggal 2 Januari- 17 Februari 2018. Pada PKL ini saya sebagai Video Editor
                  yang mengedit video-video seperti video pernikahan, video prewedding, video ulang tahun, sampai dengan video keagamaan</p>
              </div>
            </li>
            <li>
              <div class="title">2018</div>
              <div class="details">
                <h5>Praktek Kerja Lapangan</h5>
                <small class="fg-theme">Ganeshcom</small>
                <p>Praktek Kerja Lapangan dimulai dari tanggal 16 Juli- 25 Agustus 2018. Pada PKL ini saya sebagai Tenaga Multimedia yang membantu
                  mengedit video-video yang berkaitan dengan Ganeshcom.
                </p>
              </div>
            </li>
            <li>
              <div class="title">2020-2021</div>
              <div class="details">
                <h5>Organisasi Mahasiswa</h5>
                <small class="fg-theme">HMJ Teknik Informatika Undiksha</small>
                <p>Dikepengurusan tahun ini saya menjabat sebagai anggota Sub Bidang Informasi dan Komunikasi Bidang 5 Teknologi HMJ TI Undiksha
                </p>
              </div>
            </li>
            <li>
              <div class="title">2021-2022</div>
              <div class="details">
                <h5>Organisasi Mahasiswa</h5>
                <small class="fg-theme">HMJ Teknik Informatika Undiksha</small>
                <p>Dikepengurusan tahun ini saya menjabat sebagai Koordinator Sub Bidang Multimedia Bidang 5 Teknologi HMJ TI Undiksha
                </p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

@endsection