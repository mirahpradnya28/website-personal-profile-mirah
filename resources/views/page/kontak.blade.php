@extends('master.app')
@section('konten')
    <!-- Caption header -->
    <div class="caption-header text-center wow zoomInDown">
      <h5 class="fw-normal">KONTAK</h5>
      <h1 class="fw-light mb-4">Saya <b class="fg-theme">Mirah</b> Pradnya</h1>
      <div class="badge">Sistem Informasi'19 Undiksha</div>
    </div> 
    <!-- End Caption header -->
  </div>
  
    <!-- Kontak -->
    <div class="vg-footer">
        <h1 class="text-center">Kontak Saya</h1>
        <br>
        <div class="container">
        <div class="row">
            <div class="col-lg-4 py-3">
            <div class="footer-info">
                <p>Dimana anda bisa mencari saya?</p>
                <hr class="divider">
                <p class="fs-large fg-white">Kelurahan Penatih Dangin Puri, Kecamatan Denpasar Timur, Kota Denpasar</p>
            </div>
            </div>
            <div class="col-md-6 col-lg-3 py-3">
                <div class="float-lg-right">
                    <p>Ikuti Sosial Media Saya</p>
                    <hr class="divider">
                    <ul class="list-unstyled">
                    <li><a href="https://www.instagram.com/mirahpradnya28_">Instagram</a></li>
                    <li><a href="https://web.facebook.com/mirah.pradnya28/">Facebook</a></li>
                    <li><a href="https://twitter.com/mirah_pradnya91?s=09">Twitter</a></li>
                    <li><a href="https://www.youtube.com/channel/UCpQ6s74Ba-TbmSUa75fok6w">Youtube</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 py-3">
                <div class="float-lg-right">
                    <p>Hubungi Saya</p>
                    <hr class="divider">
                    <ul class="list-unstyled">
                    <li>082147159570</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- End Kontak -->

@endsection