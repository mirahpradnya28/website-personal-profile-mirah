<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[App\Http\Controllers\BerandaController::class,'index'])->name('beranda');
Route::get('beranda-saya',[App\Http\Controllers\BerandaController::class,'index'])->name('beranda');
Route::get('tentang-saya',[App\Http\Controllers\TentangController::class,'index'])->name('tentang');
Route::get('skill-saya',[App\Http\Controllers\SkillController::class,'index'])->name('skill');
Route::get('pendidikan-saya',[App\Http\Controllers\PendidikanController::class,'index'])->name('pendidikan');
Route::get('galeri-saya',[App\Http\Controllers\GaleriController::class,'index'])->name('galeri');
Route::get('kontak-saya',[App\Http\Controllers\KontakController::class,'index'])->name('kontak');














// Route::get('/', function () {
//     $page = 1;
//     return view('page.beranda', compact('page'));
// })->name('beranda');

// Route::get('/beranda', function () {
//     $page = 1;
//     return view('page.beranda', compact('page'));
// })->name('beranda');

// Route::get('/tentang', function () {
//     $page = 2;
//     return view('page.tentang', compact('page'));
// })->name('tentang');

// Route::get('/skill', function () {
//     $page = 3;
//     return view('page.skill', compact('page'));
// })->name('skill');

// Route::get('/pendidikan', function () {
//     $page = 4;
//     return view('page.pendidikan', compact('page'));
// })->name('pendidikan');
    
// Route::get('/galeri', function () {
//     $page = 5;
//     return view('page.galeri', compact('page'));
// })->name('galeri');

// Route::get('/kontak', function () {
//     $page = 6;
//     return view('page.kontak', compact('page'));
// })->name('kontak');
